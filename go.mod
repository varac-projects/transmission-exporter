module github.com/metalmatze/transmission-exporter

go 1.19

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/joho/godotenv v1.5.1
	github.com/prometheus/client_golang v0.9.4
)

require (
	github.com/alexflint/go-scalar v1.1.0 // indirect
	github.com/beorn7/perks v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gogo/protobuf v1.1.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90 // indirect
	github.com/prometheus/common v0.4.1 // indirect
	github.com/prometheus/procfs v0.0.2 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
)
